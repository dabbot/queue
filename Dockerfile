FROM node:11.2.0-alpine as build

WORKDIR /app

COPY ./src ./src
COPY ./package.json ./package.json
COPY ./yarn.lock ./yarn.lock

RUN yarn install

ENTRYPOINT node ./src/index.js
