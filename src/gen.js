function guildQueue(guildId) {
    return `g:${guildId}:q`;
}

function songTrackId(songId) {
    return `s:${songId}`;
}

function trackSongId(track) {
    return `t:${track}`;
}

module.exports = {
    guildQueue,
    songTrackId,
    trackSongId,
};
