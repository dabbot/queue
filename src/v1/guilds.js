const uuidv4 = require('uuid/v4');

const gen = require('../gen');

async function getOrCreateSong(redis, track) {
    const trackKey = gen.trackSongId(track);
    const id = await redis.get(trackKey);

    if (id) {
        return id;
    }

    const newId = uuidv4();

    const pipeline = redis.pipeline();
    redis.set(trackKey, newId);
    redis.set(gen.songTrackId(newId), track);
    await pipeline.exec();

    return newId;
}

async function replaceQueue(redis, key, newQueueItems) {
    const pipeline = redis.pipeline();
    pipeline.del(key);
    pipeline.rpush(key, newQueueItems);
    await pipeline.exec();
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));

        [array[i], array[j]] = [array[j], array[i]];
    }

    return array;
}

async function add(data, redis) {
    const { guild_id: guildId, track } = data;
    const songId = await getOrCreateSong(redis, track);
    await redis.rpush(gen.guildQueue(guildId), songId);

    return [201, JSON.stringify({
        'guild_id': guildId,
        'song_id': songId,
        'track': track,
    })];
}

async function addBatch(data, redis) {
    const { guild_id: guildId, tracks } = data;
    const insertions = tracks.map(async track => {
        const songId = await getOrCreateSong(redis, track);
        await redis.rpush(gen.guildQueue(guildId), songId);

        return {
            'guild_id': guildId,
            'song_id': songId,
            'track': track,
        };
    });

    const songIdArray = await Promise.all(insertions);

    console.log(songIdArray);

    return [201, JSON.stringify(songIdArray)];
}

async function del(data, redis) {
    const { guild_id: guildId, index } = data;

    const key = gen.guildQueue(guildId);

    let items = await redis.lrange(key, 0, -1);
    items.splice(index, 1);

    await replaceQueue(redis, key, items);

    return [201, JSON.stringify(items)];
}

async function deleteQueue(data, redis) {
    await redis.del(gen.guildQueue(data['guild_id']));

    return [204, ''];
}

async function get(data, redis) {
    let { guild_id: guildId, limit, offset } = data;
    offset = offset || 0;
    limit = typeof limit === 'undefined' ? offset + 9 : offset + limit - 1;

    if (limit === 0) {
        return [200, '[]'];
    }

    const key = gen.guildQueue(guildId);
    let ids = await redis.lrange(key, offset, limit);

    if (ids.length === 0) {
        return [200, '[]'];
    }

    const tracks = await redis.mget(ids.map(gen.songTrackId));

    return [200, JSON.stringify(tracks)];
}

function index() {
    return [200, 'Hello'];
}

async function pop(data, redis) {
    const { guild_id: guildId } = data;
    const key = gen.guildQueue(guildId);
    const songId = await redis.lpop(key);

    const track = await redis.get(gen.songTrackId(songId));

    return [201, JSON.stringify(track)];
}

async function reorder(data, redis) {
    const { guild_id: guildId } = data;
    let { from, to } = data;

    if (from == to) {
        return [400, "The two positions are the same."];
    }

    // If `from` is higher than `to`, switch them.
    if (from > to) {
        [from, to] = [to, from];
    }

    let key = gen.guildQueue(guildId);
    let ids = await redis.lrange(key, 0, -1);

    if (ids.length == 0) {
        return [400, "There aren't any songs in the queue."];
    }

    const topIdx = ids.length - 1;

    if (to > topIdx) {
        to = topIdx;
    }

    // If `from` is too high, then it's a no-op.
    if (from >= topIdx) {
        return [400, "There aren't that many songs."];
    }

    // Some sample valid values of `from` and `to`, with a total of 15 items
    // (with the last index being 14):
    //
    // - 4, 10
    // - 10, 4
    // - 13, 14
    // - 14, 2

    // Copy the value of `to`, remove the index, then add it at the new
    // index.
    const value = ids.splice(from, 1);
    // Subtract 1 from `to` due to `from` being removed.
    const before = ids.splice(0, to);
    const after = ids;

    const newIds = before.concat([value]).concat(after);
    const pipeline = redis.pipeline();
    pipeline.del(key);
    pipeline.rpush(key, newIds);
    await pipeline.exec();

    return [201, ''];
}

async function shuffle(data, redis) {
    const { guild_id: guildId } = data;

    const key = gen.guildQueue(guildId);

    const items = await redis.lrange(key, 0, -1);
    const shuffled = shuffleArray(items);

    await replaceQueue(redis, key, shuffled);

    return [200, JSON.stringify(shuffled)];
}

module.exports = {
    add,
    addBatch,
    del,
    deleteQueue,
    get,
    index,
    pop,
    reorder,
    shuffle,
};
